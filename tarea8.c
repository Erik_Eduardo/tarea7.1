#include <stdio.h>
/*
despliega el tamaño de las variables en memoria hail aron
*/

int main(int argc, char const *argv[]) {
  printf("un int ocupa: %lu bytes en memoria\n",sizeof(int));
  printf("un float ocupa: %lu bytes en memoria\n",sizeof(float));
  printf("un char ocupa: %lu bytes en memoria\n",sizeof(char));
  printf("un long int ocupa: %lu bytes en memoria\n",sizeof(long int));

  return 0;
}
